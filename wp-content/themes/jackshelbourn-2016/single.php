<?php 
	get_header(); 
	$categories = get_the_terms( $post->ID , 'category' );
?>

	<section class="container ultra narrow">

		<article>
			<span class="date-box"><i class="fa fa-calendar"></i> <?php the_time('jS F Y'); ?></span>
			<?php the_post_thumbnail('post-feature'); ?>
			
			<h1 class="page-title"><?php the_title(); ?></h1>
			<div class="article-content">
				<?php the_content(); ?>
			</div>
			<div class="category-list">
				<?php foreach($categories as $category) { ?>
					<a href="<?php bloginfo('url'); ?>/resources?category=<?php echo $category->slug; ?>" class="button standard small"><?php echo $category->name; ?></a>
				<?php } ?>
			</div>
		</article>

		<?php get_template_part('snippets/nav', 'posts'); ?>

	</section>

<?php get_footer(); ?>
