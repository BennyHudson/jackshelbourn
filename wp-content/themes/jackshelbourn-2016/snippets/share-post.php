<ul class="sharers">
	<li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="_blank" class="sharer facebook">Share <i class="fa fa-facebook"></i></a></li>
	<li><a href="https://twitter.com/intent/tweet?text=<?php the_title(); ?>&url=<?php the_permalink(); ?>" class="sharer twitter" target="_blank">Tweet <i class="fa fa-twitter"></i></a></li>
	<li><a href="https://plus.google.com/share?url=<?php the_permalink(); ?>" target="_blank" class="sharer google">Plus <i class="fa fa-google-plus"></i></a></li>
</ul>
