<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/lib/jquery-1.8.2.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/lib/modernizr-custom.js" type="text/javascript"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/lib/matchMedia.js" type="text/javascript"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/lib/matchMedia.addListener.js" type="text/javascript"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/lib/enquire.min.js" type="text/javascript"></script>
<link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i" rel="stylesheet">
<meta charset="<?php bloginfo('charset'); ?>">
<meta name="viewport" content="width=device-width"/>  
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<?php get_template_part('includes/include', 'favicon'); ?>
<title><?php bloginfo('name'); ?></title>
<?php wp_head(); ?>
<!-- Begin Cookie Consent plugin by Silktide - http://silktide.com/cookieconsent -->
<script type="text/javascript">
    window.cookieconsent_options = {"message":"This website uses cookies to ensure you get the best experience on our website","dismiss":"Got it!","learnMore":"More info","link":null,"theme":"dark-bottom"};
</script>

<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/1.0.9/cookieconsent.min.js"></script>
<!-- End Cookie Consent plugin -->

</head>
<body <?php body_class('wedo'); ?>>

	<div class="overlay-cover"></div>
	<div class="overlay-wrap" id="contact-overlay">
		<div class="overlay-container">
			<div class="overlay-content">
				<div class="overlay-form">
					<a href="#" class="overlay-close">Close</a>
					<div class="overlay-logo"></div>
					<?php echo do_shortcode('[contact-form-7 id="4" title="Quick Contact"]'); ?>
				</div>
			</div>
		</div>
	</div>

	<section class="page-content">
		<section class="footer-fix">


			<?php if(is_front_page()) { ?>
				<header class="feature-header">
			<?php } else { ?>
				<header class="main-header">
			<?php } ?>
				<section class="container">
					<?php get_template_part('snippets/content', 'logo'); ?>
					<nav>
						<?php wp_nav_menu( array('theme_location' => 'header-menu') ); ?>
					</nav>
				</section>
			</header>
