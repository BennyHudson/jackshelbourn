$(document).ready(function() {

	jQuery('.container').fitVids();

    var windowHeight = $( window ).height();
	var footerHeight = $('footer').outerHeight();
    var headerHeight = $('header.main-header').outerHeight();
    $('.footer-fix').css({
        'padding-bottom' : footerHeight,
        'padding-top': headerHeight
    });

    $('.home-video').css('height', windowHeight);
    jQuery('.feature-header').midnight();

    $('.overlay-trigger').click(function(e) {

        var target = $(this).data('target');

        e.preventDefault();
        $('html').css('overflow', 'hidden');
        $('.overlay-cover, #' + target).show().css('visibility', 'visible');

        var targetHeight = $('#' + target).find('.overlay-content').outerHeight();

        if(windowHeight > targetHeight) {
            $('.overlay-container').addClass('flex');
        }

    });
    
    $('.overlay-close').click(function(e) {
        e.preventDefault();
        $('html').css('overflow', 'auto');
        $('.overlay-cover, .overlay-wrap').hide();
        $('.overlay-container').removeClass('flex');

        var inputs = $('input, textarea, select').not(':input[type=button], :input[type=submit], :input[type=reset]');

        $(inputs).val("");
    });

});

enquire
    .register("screen and (min-width:31em)", function() { 
        $(document).ready(function() {
            
        });
    }, true)
    .register("screen and (max-width:31em)", function() { 
        $(document).ready(function() {
            
        });
    });

var ieVersion = null;
if (document.body.style['msFlexOrder'] != undefined) {
    ieVersion = "ie10";
    $('html').addClass('ie-10');
}
if (document.body.style['msTextCombineHorizontal'] != undefined) {
    ieVersion = "ie11";
    $('html').removeClass('ie-10').addClass('ie-11');
}
