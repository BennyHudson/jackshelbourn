<?php
	 
	add_action( 'after_setup_theme', 'tastic_theme_setup' );
	function tastic_theme_setup() {
		include_once( get_stylesheet_directory() . '/acf/acf.php' );
		add_filter('acf/settings/path', 'my_acf_settings_path');
		add_filter('acf/settings/dir', 'my_acf_settings_dir');
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'post-thumbnails' );
		add_action( 'widgets_init', 'tastic_register_sidebars' );
		add_filter('wp_mail_from_name', 'new_mail_from_name');
		add_action( 'init', 'register_my_menus' );
		add_action( 'login_enqueue_scripts', 'my_login_logo' );
		add_action('login_head', 'add_favicon');
		add_action('admin_head', 'add_favicon');
		add_action('init', 'tastic_posts');
		add_action('init', 'tastic_taxonomies');
		add_action('init', 'flush_rewrite_rules');
		add_action( 'wp_enqueue_scripts', 'tastic_scripts' );
		add_filter('admin_footer_text', 'tastic_footer');
		add_action('wp_dashboard_setup', 'tastic_dashboard');
		add_action('admin_head', 'tastic_admin');
		add_action('wp_before_admin_bar_render', 'annointed_admin_bar_remove', 0);
		add_filter( 'wpseo_metabox_prio', function() { return 'low';});
		add_image_size( 'gallery-thumb', 300, 240, true );
		add_image_size( 'post-feature', 900, 450, true );
	}

	get_template_part('functions/include', 'favicons');
	get_template_part('functions/include', 'postnav');
	get_template_part('functions/include', 'menus');
	get_template_part('functions/include', 'scripts');
	get_template_part('functions/include', 'sidebar');
	get_template_part('functions/include', 'cpts');
	get_template_part('functions/include', 'email');
	get_template_part('functions/include', 'acf');
	get_template_part('functions/include', 'footer');
	get_template_part('functions/include', 'welcome');
	get_template_part('functions/include', 'adminstyle');
	get_template_part('functions/include', 'gallery'); 

	remove_action('wp_head', 'rsd_link');
	remove_action('wp_head', 'wp_generator');
	remove_action('wp_head', 'feed_links', 2);
	remove_action('wp_head', 'index_rel_link');
	remove_action('wp_head', 'wlwmanifest_link');
	remove_action('wp_head', 'feed_links_extra', 3);
	remove_action('wp_head', 'start_post_rel_link', 10, 0);
	remove_action('wp_head', 'parent_post_rel_link', 10, 0);
	remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);

?>
