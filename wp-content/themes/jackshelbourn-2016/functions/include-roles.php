<?php

	$result = add_role( 'site_owner', __(
	'Site Owner' ),
	array( 

		// Dashboard
		'read' => true, // Access to Dashboard and Users -> Your Profile.
		'update_core' => false, // Can NOT update core. I added a plugin for this.
		'edit_posts' => true, //Access to Posts, Add New, Comments and moderating comments.

		// Posts
		'edit_posts' => true, //Access to Posts, Add New, Comments and moderating comments.
		'create_posts' => true, // Allows user to create new posts
		'delete_posts' => true, // Can delete posts.
		'publish_posts' => true, // Can publish posts. Otherwise they stay in draft mode.
		'delete_published_posts' => true, // Can delete published posts.
		'edit_published_posts' => true, // Can edit posts.
		'edit_others_posts' => true, // Can edit other users posts.
		'delete_others_posts' => true, // Can delete other users posts.

		// Categories, comments and users
		'manage_categories' => true, // Access to managing categories.
		'moderate_comments' => true, // Access to moderating comments. Edit posts also needs to be set to true.
		'edit_comments' => true, // Comments are blocked out for this user.
		'edit_users' => true, // Can not view other users.
		'create_users' => true,
		'list_users' => true,
		'promote_users' => true,
		'remove_users' => true,

		// Pages
		'edit_pages' => true, // Access to Pages and Add New (page).
		'publish_pages' => true, // Can publish pages.
		'edit_other_pages' => true, // Can edit other users pages.
		'edit_published_ pages' => true, // Can edit published pages.
		'delete_pages' => true, // Can delete pages.
		'delete_others_pages' => true, // Can delete other users pages.
		'delete_published_pages' => true, // Can delete published pages.

		// Media Library
		'upload_files' => true, // Access to Media Library.

		// Appearance
		'edit_themes_options' => false, // Access to Appearance panel options.
		'switch_themes' => false, // Can not switch themes.
		'delete_themes' => false, // Can NOT delete themes.
		'install_themes' => false, // Can not install a new theme.
		'update_themes' => false, // Can NOT update themes.
		'edit_themes' => false, // Can not edit themes - through the appearance editor.

		// Plugins
		'activate_plugins' => false, // Access to plugins screen.
		'edit_plugins' => false, // Can not edit plugins - through the appearance editor.
		'install_plugins' => false, // Access to installing a new plugin.
		'update_plugins' => false, // Can update plugins.
		'delete_plugins' => false, // Can NOT delete plugins.

		// Settings
		'manage_options' => false, // Can not access Settings section.

		// Tools
		'import' => false, // Can not access Tools section.

	) );

?>
