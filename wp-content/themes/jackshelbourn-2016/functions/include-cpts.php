<?php
	function tastic_posts() {
		
		register_post_type(
			'works', 
			array(
				'labels'		=> array (
									'name'					=> _x('Works', 'post type general name'),
									'singular_name'			=> _x('Work', 'post type singular name'),
									'add_new'				=> _x('Add new work', 'book'),
									'add_new_item'			=> ('Add New Work'),
									'edit_item'				=> ('Edit Work'),
									'new_item'				=> ('New Work'),
									'all_items'				=> ('All Works'),
									'view_item'				=> ('View Works'),
									'search_items'			=> ('Search Works'),
									'not_found'				=> ('No Works found'),
									'not_found_in_trash'	=> ('No Works found in trash'),
									'parent_item_colon'		=> '',
									'menu_name'				=> 'Works'
								),
				'description'	=> 'Holds all the Works',
				'public'		=> true,
				'menu_position'	=> 19,
				'menu_icon'		=> 'dashicons-format-status',
				'supports'		=> array('title', 'editor', 'excerpt', 'revisions', 'thumbnail'),
				'has_archive'	=> true
			)
		);
	
	}
	function tastic_taxonomies() {
		/*
			register_taxonomy( 
				'publication-type', 
				'publications', 
				array(
		            'labels' 		=> array(
							 			'name'              => _x( 'Publication Type', 'taxonomy general name' ),
							            'singular_name'     => _x( 'Publication Type', 'taxonomy singular name' ),
							            'search_items'      => __( 'Search Publication Types' ),
							            'all_items'         => __( 'All Publication Types' ),
							            'parent_item'       => __( 'Parent Publication Type' ),
							            'parent_item_colon' => __( 'Parent Publication Type:' ),
							            'edit_item'         => __( 'Edit Publication Type' ), 
							            'update_item'       => __( 'Update Publication Type' ),
							            'add_new_item'      => __( 'Add New Publication Type' ),
							            'new_item_name'     => __( 'New Publication Type' ),
							            'menu_name'         => __( 'Publication Types' ),
							        ),
		            'hierarchical' => true,
		        )
			);
		*/
    }
?>
