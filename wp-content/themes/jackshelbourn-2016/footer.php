			<footer>
				<section class="container">
					<p>&copy; <?php echo date("Y") ?> Jack Shelbourn | Website by <a href="http://wedo.digital">wedo.digital</a></p>
				</section>
			</footer>
		</section>
	</section>
	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyBR5bX6m_CEPwitun65XjrFWYZVRtzqADA"></script>
	<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/js/font-awesome/css/font-awesome.min.css">

	<!--Google Analytics-->

	<?php wp_footer(); ?>
</body>
</html>
