<?php
	//Template Name: Landing Page
	get_header();
?>

	<section class="home-video">
		<h2>some sort of bullshit tagline</h2>
		<video autoplay loop poster="<?php echo $image[0]; ?>" preload="auto" id="bgvid">
			<source type="video/webm" src="<?php echo get_stylesheet_directory_uri(); ?>/video/homepage-feature.webm">
			<source type="video/mp4" src="<?php echo get_stylesheet_directory_uri(); ?>/video/homepage-feature.mp4">
		</video>
	</section>

	<section class="home-main" data-midnight="solid">
		<section class="home-content">
			<section class="container narrow ultra">
				<?php the_content(); ?>
			</section>
		</section>
	</section>

<?php get_footer(); ?>
