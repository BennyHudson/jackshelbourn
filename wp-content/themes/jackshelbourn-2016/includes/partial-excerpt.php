<div class="feature-clip">
	<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('thumbnail'); ?></a>
</div>
<div class="title-clip">
	<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
</div>
<div class="excerpt-clip">
	<?php the_excerpt(); ?>
</div>
<a href="<?php the_permalink(); ?>" class="read-more">Read More</a>
